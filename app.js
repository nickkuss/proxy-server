const app = require("express")()
const { createProxyMiddleware } = require("http-proxy-middleware")
const https = require("https")
const http = require("http")
const fs = require("fs")
const cors = require("cors")

app.use(cors())

const proxy = (endpoint, targetUrl) => {
  return createProxyMiddleware(endpoint, {
    target: targetUrl,
    changeOrigin: true,
    pathRewrite: { [`${endpoint}`]: "" },
    secure: false,
    logLevel: "silent",
  })
}
//Courier
app.use(proxy("/tiki", "http://apis.mytiki.net:8321"))
app.use(proxy("/jne", "http://apiv2.jne.co.id:10101"))
app.use(proxy("/jnt", "http://159.138.2.151:22223"))
//Virtual Account
app.use(proxy("/bni-va", "https://apibeta.bni-ecollection.com"))
//IP Check
app.use(proxy("/ip-check", "https://www.icanhazip.com"))
app.use(proxy("/ip-info", "https://www.infobyip.com/"))
//Merchanel Heroku
app.use(proxy("/heroku", "http://merchanel-dev.herokuapp.com"))

app.listen(3000, () => {
  console.log("App listening on port 3000.")
})

// Listen both http & https ports
const httpServer = http.createServer(app)
const httpsServer = https.createServer(
  {
    key: fs.readFileSync("/etc/letsencrypt/live/px.merchanel.com/privkey.pem"),
    cert: fs.readFileSync(
      "/etc/letsencrypt/live/px.merchanel.com/fullchain.pem"
    ),
  },
  app
)

httpServer.listen(80, () => {
  console.log("HTTP Server running on port 80")
})

httpsServer.listen(443, () => {
  console.log("HTTPS Server running on port 443")
})
